INSTDIR  = /usr/local/share
ICONDIR0 = Dialog
ICONDIR1 = Desktop
ICONDIR2 = WindowManager

.PHONY: install uninstall

install:
	install -d $(INSTDIR)/DeskViewIcons/$(ICONDIR0)
	install -d $(INSTDIR)/DeskViewIcons/$(ICONDIR1)
	install -d $(INSTDIR)/DeskViewIcons/$(ICONDIR2)
	
	cp -r $(ICONDIR0)/* $(INSTDIR)/DeskViewIcons/$(ICONDIR0)/
	cp -r $(ICONDIR1)/* $(INSTDIR)/DeskViewIcons/$(ICONDIR1)/
	cp -r $(ICONDIR2)/* $(INSTDIR)/DeskViewIcons/$(ICONDIR2)/

uninstall:
	rm -rf $(INSTDIR)/DeskViewIcons