
### DeskView Icons

IRIX/BeOS inspired 48x48 desktop icons and some 87x67 icons for the Motif Window Manager.

![screenshot](screenshots/dvicons_sample0.png)

Install to /usr/local/share/DeskViewIcons:

```
sudo make install
```

Uninstall:

```
sudo make uninstall
```

#### License
These images are distributed free of charge under the BSD Zero Clause license.